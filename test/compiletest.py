# rfmdocstart: spack-test
import reframe as rfm
import reframe.utility.sanity as sn


@rfm.simple_test
class SpackTest(rfm.RegressionTest):
  descr = 'Demo test using Spack to build the test code'
  valid_systems = ['*:milan-bxi', '*:rome-bxi']
  valid_prog_environs = ['gnu-11']
  executable = './matar'
  executable_opts = ['-x 1000', '-y 1000', '-d']
  build_system = 'CMake'
  exclusive_access = True

  num_procs = parameter([1, 2, 4, 8, 16])
  num_threads = parameter([1, 2, 4, 8, 16, 32, 64, 128])
  bindings = parameter(['spread'])

  variables = {
    "OMP_NUM_THREADS": "1",
    "OMP_PLACES": "",
    "OMP_PROC_BIND": ""
  }


  @run_before('compile')
  def setup_build_system(self):
    self.build_system.max_concurrency = 128
    self.build_system.config_opts = [\
      '-S /ccc/work/cont001/ocre/palezisc/matar-flex/',
      '-DCMAKE_BUILD_TYPE=Release',
      '-Dadiak_DIR=/ccc/work/cont001/ocre/palezisc/matar-flex/install_tree/linux-rhel8-zen3/gcc-11.1.0/adiak-0.2.2-qvcsu2i4uyvdcir66kjl5h4iro3h5y3h/lib/cmake/adiak/']
  
  @run_before("run")
  def set_num_threads(self):
    self.num_cpus_per_task = self.num_threads
    self.variables["OMP_NUM_THREADS"] = str(self.num_threads)

  @run_before('run')
  def set_num_procs(self):
    self.num_tasks = self.num_procs
    self.num_tasks_per_node = 1


  @sanity_function
  def assert_size(self):
    return sn.assert_found(r'(1000x1000)', self.stderr)

  @performance_function("s", perf_key="ElapsedTime")
  def extract_elapsed_time(self):
    return sn.extractsingle(r"Elapsed\stime\s=\s(\S+)\sseconds", self.stderr, 1, float)

# rfmdocend: spack-test
