#!/usr/bin/env python3

import sys
import argparse
import re
import os,glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

def get_raw_data(partition : str, backend : str, size : str):
  data = []
  prefix = 'saved_perflogs/' + backend 
  print("Plotting from " + prefix + " logs")
  for filename in glob.glob(os.path.join(prefix, '*', partition, '*.log')):
    size = filename.split('/')[2].split('x')[0]
    with open(filename, "r") as f:
      lines = f.readlines()
      for line in lines:
        line += '|' + size
        data.append(line.split('|'))

  return data

def clean_data(data : list, inplace : bool = False):

  data = data if inplace else deepcopy(data)

  for row in data:
    row[0] = row[2].split()[4]
    row[1] = row[2].split()[1]
    row[3] = row[2].split()[3]
    row[2] = row[2].split()[2]

    row.pop(6)
    row.pop(6)

    row[0] = row[0].split(':')[1]
    row[1] = row[1].split('=')[1]
    row[2] = row[2].split('=')[1]
    row[3] = row[3].split('=')[1]
    row[4] = row[4].split('=')[1]
    row[5] = row[5].split('=')[1]

  return data

def process_data(
  data : list,
  shared_memory : bool,
  distributed_memory : bool):
  
  cols=['partition', 'processus', 'threads', 'binding', 'tasks', 'time', 'size']
  df = pd.DataFrame(data, columns = cols )

  df['processus'] = df['processus'].astype(int)
  df['threads'] = df['threads'].astype(int)
  df['tasks'] = df['tasks'].astype(int)
  df['time'] = df['time'].astype(float)
  df['size'] = df['size'].astype(int)

  df = df[df.processus != 10]
  df = df[df.processus != 12]
  df = df[df.processus != 14]

  if not distributed_memory:
    df = df[df.processus != 2]
    df = df[df.processus != 4]
    df = df[df.processus != 8]
    df = df[df.processus != 16]

  if not shared_memory:
    df = df[df.threads != 2]
    df = df[df.threads != 4]
    df = df[df.threads != 8]
    df = df[df.threads != 16]
    df = df[df.threads != 32]
    df = df[df.threads != 64]
    df = df[df.threads != 128]

  df = df.groupby(np.arange(len(df))//10).mean()

  df['processus'] = df['processus'].astype(int)
  df['threads'] = df['threads'].astype(int)
  df['tasks'] = df['tasks'].astype(int)
  df['size'] = df['size'].astype(int)

  ref_seq_thre = df.loc[df['threads'] == 1, ['time']]
  ref_seq_proc = df.loc[df['processus'] == 1, ['time']]
  ref = pd.merge(ref_seq_proc, ref_seq_thre)

  ref.sort_values(by=['time'], inplace=True)
  ref['size'] = sorted(list(df['size'].drop_duplicates()))
  ref.rename(columns={'time' : 'ref'}, inplace=True)
  df = df.merge(ref, on='size')
  df['speedup'] = df['ref'] / df['time']

  return df

def plot_data(
  df,
  partition: str, 
  x : str, 
  y : str, 
  compare : str, 
  logx : bool,
  logy : bool, 
  title : str, 
  inplace : bool = False):

  df = df if inplace else deepcopy(df)
  
  df.sort_values(by=[x], inplace=True)

  df.set_index(x, inplace=True)
  df.groupby(compare)[y].plot(
    legend = True,
    figsize = (10, 6),
    grid = True,
    xlabel = x.capitalize(),
    ylabel = y.capitalize(),
    logx = logx,
    logy = logy,
    title = title if title != ' '
      else y + ' en fonction de ' + x + ' sur la partition ' + partition)

  plt.show()
  return df

def parse_arguments(arguments):
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)

  parser.add_argument('--partition',
    '-p',
    type=str, 
    help="Input partition's name",
    default='rome-bxi')
  parser.add_argument('-x',
    type=str, 
    help="Input x-axis category",
    default='processus')
  parser.add_argument('-y',
    type=str, 
    help="Input y-axis category",
    default='speedup')
  parser.add_argument('--compare',
    '-c',
    type=str, 
    help="Input different categories to compare",
    default='threads')
  parser.add_argument('--logx',
    '-lx',
    help="Enable logarithmic on x axis",
    action = 'store_true',
    default=False)
  parser.add_argument('--logy',
    '-ly',
    help="Enable logarithmic on y axis",
    action = 'store_true',
    default=False)
  parser.add_argument('--title',
    '-t',
    type=str, 
    help="Input the title for plotted graph",
    default=' ')
  parser.add_argument('--backend',
    '-b',
    type=str, 
    help="Input the backend name",
    default='mpi')
  parser.add_argument('--size',
    '-s',
    type=str, 
    help="Input the size of the problem",
    default='1000')
  parser.add_argument('--shared',
    '-sm',
    help="Enable shared memory data",
    action = 'store_true',
    default=False)
  parser.add_argument('--distributed',
    '-dm',
    help="Enable distributed memory data",
    action = 'store_true',
    default=False)

  return parser.parse_args(arguments)

def main(arguments):
  
  args = parse_arguments(arguments)

  data = get_raw_data(args.partition, args.backend, args.size)
  data = clean_data(data)
  data = process_data(data, args.shared, args.distributed)
  print(plot_data(data,
    args.partition,
    args.x, args.y,
    args.compare,
    args.logx, args.logy,
    args.title))


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
