/*---------------------------------------------------------------------------*/
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (C) 2022 Clément Palézis                                        */
/*---------------------------------------------------------------------------*/

#include <fmt/core.h>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "FileUtils.hpp"
#include "mesh.hpp"
#include "OutputManager.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

OutputManager::OutputManager(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor)
  : m_mesh(mesh_accessor),
    m_field(field_accessor)
{
  if (od_name.value() != "") {
    m_output_directory = fmt::format("{}/{}", "data", od_name.value());
  }
  else {
    m_output_directory = fmt::format("{}/{}", "data", timestamp());
  }

  if (flecsi::process() == 0) {
    mkdir(m_output_directory);
    fmt::print("[OutputManager] Results will be stored in '{}'\n",
      m_output_directory);
  }

  register_fields();
  register_writer();
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::set_mesh_and_field(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor)
{
  m_mesh = mesh_accessor;
  m_field = field_accessor;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::set_output_directory(const std::string& output_directory)
{
  m_output_directory = output_directory;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::force_output()
{
  m_force_output = true;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::write(int iteration, double time)
{
  // Check if an output is wanted, return immediately if not.
  if (!activate_output(iteration, time)) return;

  copy_fields(iteration);

  // std::visit([this, &iteration, &time](const auto& writer) {
  std::visit([this, &iteration, &time](auto& writer) {
    using T = std::decay_t<decltype(writer)>;
    if constexpr (!std::is_same_v<std::monostate, T>) {
      writer.write(m_fields, iteration, time);
    }
  }, m_writer_variant);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::register_fields()
{
  size_t field_size;

  if(dump_vtk.value() == "normal") {
    field_size = m_mesh.size<mesh::index_space::cells, mesh::y_axis, mesh::range::logical>()
               * m_mesh.size<mesh::index_space::cells, mesh::x_axis, mesh::range::logical>();
  }
  else {
    field_size = m_mesh.size<mesh::index_space::cells, mesh::y_axis>()
               * m_mesh.size<mesh::index_space::cells, mesh::x_axis>();
  }

  m_fields["Heat"].resize(field_size);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::register_writer()
{
  if(dump_vtk.value() == "normal") {
    m_writer_variant.emplace<VTKWriter>(m_mesh, m_field, m_output_directory,
      "heat_equation", flecsi::process(), flecsi::processes());
  }
  else {
    m_writer_variant.emplace<DebugVTKWriter>(m_mesh, m_field,
    m_output_directory, "debug_heat_equation",
    flecsi::process(), flecsi::processes());
  }

  if (flecsi::process() == 0) {
    fmt::print("[OutputManager|{}] Registered writer\n", flecsi::process());
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void OutputManager::copy_fields(int it)
{
  auto val = m_mesh.mdspan<mesh::cells>(m_field);

  std::size_t nb_cells_j;
  std::size_t nb_cells_i;

  if (dump_vtk.value() == "normal") {
    nb_cells_i = m_mesh.size<mesh::index_space::cells, mesh::x_axis, mesh::range::logical>();
    for (auto j : m_mesh.cells<mesh::y_axis, mesh::range::logical>()) {
      for (auto i : m_mesh.cells<mesh::x_axis, mesh::range::logical>()) {
        const auto cell_idx = (size_t)i - 1 + ((size_t)j - 1) * nb_cells_i;
        m_fields["Heat"][cell_idx] = val[j][i][it % 2];
      }
    }
  }
  else {
    nb_cells_i = m_mesh.size<mesh::index_space::cells, mesh::x_axis>();
    for (auto j : m_mesh.cells<mesh::y_axis>()) {
      for (auto i : m_mesh.cells<mesh::x_axis>()) {
        const auto cell_idx = (size_t)i + (size_t)j * nb_cells_i;
        m_fields["Heat"][cell_idx] = val[j][i][it % 2];
      }
    }
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

bool OutputManager::activate_output(int iteration, double time)
{
  bool write_output_for_iteration = false;

  if (iteration % dump_freq.value() == 0 or m_force_output) {
    write_output_for_iteration = true;
  }

  return write_output_for_iteration;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
