/**
 * @file main.cpp
 * @author Clément Palézis
 * @brief Main file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <flecsi/execution.hh>
#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int main(int argc, char ** argv)
 * @brief Main function
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int
main(int argc, char ** argv) {
  auto status = flecsi::initialize(argc, argv);
  status = control::check_status(status);

  if(status != flecsi::run::status::success) {
    return status < flecsi::run::status::clean ? 0 : status;
  }

  flecsi::log::add_output_stream("clog", std::clog, true);

  auto begin = std::chrono::high_resolution_clock::now();
  status = flecsi::start(control::execute);
  auto end = std::chrono::high_resolution_clock::now();

  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
  flog(info) << "Elapsed time = " << elapsed.count() * 1e-9 << " seconds" 
             << std::endl;

  flecsi::finalize();

  return status;
} // main

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
