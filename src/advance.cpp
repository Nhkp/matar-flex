/**
 * @file advance.cpp
 * @author Clément Palézis
 * @brief Advance file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "advance.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::advance_init() {
  flecsi::log::guard guard(task::tag_advance);
  
  auto init_future = flecsi::execute<task::advance_init, flecsi::mpi>();
  
  flog(info) << init_future.get() << "(" << control::state().step() << "/"
    << nb_iter.value() << ")"<< std::endl;

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::advance() {
  annotation::rguard<advance_region> guard;

  double tolerance = 100;

  auto cell_field_accessor = cell_field(mesh_instance);

  if(dump_vtk.value() != ""){
    output_mgr->write(iteration, 0);
  }

  while ( tolerance > precision.value() ) {
    iteration++;

    flecsi::execute<task::finite_diff, flecsi::default_accelerator>(
      mesh_instance,
      cell_field_accessor,
      iteration);

    tolerance = 0;
    auto future = 
      flecsi::execute<task::max_diff, flecsi::default_accelerator>(
        mesh_instance,
        cell_field_accessor,
        tolerance);
    tolerance = future.get();

    if(dump_vtk.value() != ""){
      output_mgr->write(iteration, 0);
    }
  }

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
