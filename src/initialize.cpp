/**
 * @file initialize.cpp
 * @author Clément Palézis
 * @brief Initialize file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "initialize.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int
action::init_mesh() {
  annotation::rguard<init_region> guard;

  flog(info) << "Mesh (" << x_extents.value() << "x" << y_extents.value()
             << ") initialization" << std::endl;
  flecsi::log::flush();

  std::vector<std::size_t> cells_extents{x_extents.value(), y_extents.value()};
  std::vector<std::size_t> nodes_extents{x_extents.value() + 1, y_extents.value() + 1};

  // Distribute the number of processes over the axis colors.
  auto axis_colors = mesh::distribute(flecsi::processes(), cells_extents);

  coloring.allocate(
    axis_colors,
    cells_extents,
    nodes_extents,
    nb_ghosts.value(),
    nb_boundaries.value());

  mesh::grect geometry;
  geometry[0][0] = 0.0;
  geometry[0][1] = 1.0;
  geometry[1] = geometry[0];

  mesh_instance.allocate(coloring.get(), geometry);

  auto cell_field_accessor = cell_field(mesh_instance);

  if (init_shape.value() == "center") {
    flecsi::execute<task::apply_center_ic>(
      mesh_instance, cell_field_accessor);
  }
  else if (init_shape.value() == "corner") {
    flecsi::execute<task::apply_corner_ic>(
      mesh_instance, cell_field_accessor);
  }
  else if (init_shape.value() == "outline") {
    flecsi::execute<task::apply_outline_ic>(
      mesh_instance, cell_field_accessor);
  }
  else {
    flecsi::execute<task::apply_full_ic, flecsi::default_accelerator>(
      mesh_instance, cell_field_accessor);
  }

  if(dump_vtk.value() != "" || dump_last.value()){
    flecsi::execute<task::init_output_mgr>(
      mesh_instance, cell_field_accessor);
  }

  return 0;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
