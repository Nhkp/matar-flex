/**
 * @file init.cpp
 * @author Clément Palézis
 * @brief Init task file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "init.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::apply_corner_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor) {

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  int I = mesh_accessor.size<mesh::index_space::cells, mesh::x_axis>() - 1;
  int J = mesh_accessor.size<mesh::index_space::cells, mesh::y_axis>() - 1;

  forall(i, (mesh_accessor.cells<mesh::x_axis,
    mesh::range::interior>()), "cells") {
    val[0][J - 1][i] = (100.0/I) * i;
  };

  forall(j, (mesh_accessor.cells<mesh::y_axis,
    mesh::range::interior>()), "cells") {
    val[0][j][I - 1] = (100.0/J) * j;
  };
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::apply_outline_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor) {

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  int I = mesh_accessor.size<mesh::index_space::cells, mesh::x_axis, mesh::range::logical>();
  int J = mesh_accessor.size<mesh::index_space::cells, mesh::y_axis, mesh::range::logical>();

  int offsetI = mesh_accessor.offset<mesh::x_axis, mesh::range::logical>();
  int offsetJ = mesh_accessor.offset<mesh::y_axis, mesh::range::logical>();


  forall(i, (mesh_accessor.cells<mesh::x_axis,
    mesh::range::logical>()), "cells") {
    if (mesh_accessor.offset<mesh::y_axis, mesh::range::global>()
      == 0){
      val[offsetJ + 0][i][0] = 100;
      val[offsetJ + 1][i][0] = 100;
      val[offsetJ + 2][i][0] = 100;
      val[offsetJ + 3][i][0] = 100;
      val[offsetJ + 4][i][0] = 100;
    }

    if (mesh_accessor.size<mesh::index_space::cells, mesh::y_axis, mesh::range::global>()
      == mesh_accessor.global_id<mesh::y_axis>(J) + 1){
      val[J + offsetJ - 1][i][0] = 100;
      val[J + offsetJ - 2][i][0] = 100;
      val[J + offsetJ - 3][i][0] = 100;
      val[J + offsetJ - 4][i][0] = 100;
      val[J + offsetJ - 5][i][0] = 100;
    }
  };

  forall(j, (mesh_accessor.cells<mesh::y_axis,
    mesh::range::logical>()), "cells") {
    if (mesh_accessor.offset<mesh::x_axis, mesh::range::global>() == 0){
      val[j][offsetI + 0][0] = 100;
      val[j][offsetI + 1][0] = 100;
      val[j][offsetI + 2][0] = 100;
      val[j][offsetI + 3][0] = 100;
      val[j][offsetI + 4][0] = 100;
    }

    if (mesh_accessor.size<mesh::index_space::cells, mesh::x_axis, mesh::range::global>()
      == mesh_accessor.global_id<mesh::x_axis>(I) + 1){
      val[j][I + offsetI - 1][0] = 100;
      val[j][I + offsetI - 2][0] = 100;
      val[j][I + offsetI - 3][0] = 100;
      val[j][I + offsetI - 4][0] = 100;
      val[j][I + offsetI - 5][0] = 100;
    }
  };
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::apply_center_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor) {

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  int I = mesh_accessor.size<mesh::index_space::cells, mesh::x_axis>();
  int J = mesh_accessor.size<mesh::index_space::cells, mesh::y_axis>();
  int i,j;

  if (I % 2 == 0 && J % 2 == 0) {
    i = I / 2;
    j = J / 2;
    val[0][j][i] = 100;
    val[0][j - 1][i] = 100;
    val[0][j][i - 1] = 100;
    val[0][j - 1][i - 1] = 100;
  }
  else if (I % 2 == 0 && J % 2 == 1) {
    i = I / 2;
    j = (J - 1) / 2;

    val[0][j][i] = 100;
    val[0][j - 1][i] = 100;
    val[0][j][i - 1] = 100;
    val[0][j + 1][i] = 100;
    val[0][j + 1][i - 1] = 100;
    val[0][j - 1][i - 1] = 100;
  }
  else if (I % 2 == 1 && J % 2 == 0) {
    i = (I - 1) / 2;
    j = J / 2;

    val[0][j][i] = 100;
    val[0][j][i - 1] = 100;
    val[0][j][i + 1] = 100;
    val[0][j - 1][i] = 100;
    val[0][j - 1][i - 1] = 100;
    val[0][j - 1][i + 1] = 100;
  }
  else if (I % 2 == 1 && J % 2 == 1) {
    i = (I - 1) / 2;
    j = (J - 1) / 2;

    val[0][j][i] = 100000;
    val[0][j + 1][i] = 100000;
    val[0][j][i + 1] = 100000;
    val[0][j - 1][i] = 100000;
    val[0][j][i - 1] = 100000;
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::apply_full_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor) {
  annotation::begin<init_task>();

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  forall(j, (mesh_accessor.cells<mesh::y_axis,
    mesh::range::logical>()), "cells") {
    forall(i, (mesh_accessor.cells<mesh::x_axis,
      mesh::range::logical>()), "cells") {
      val[j][i][0] = 100;
    };
  };

  int I = mesh_accessor.offset<mesh::x_axis, mesh::range::ghost>();
  int J = mesh_accessor.offset<mesh::y_axis, mesh::range::ghost>();

  int gxsize = mesh_accessor.size<mesh::index_space::cells, mesh::x_axis, mesh::range::ghost>();
  int gysize = mesh_accessor.size<mesh::index_space::cells, mesh::y_axis, mesh::range::ghost>();

  if (gxsize != 0)
    forall(j, (mesh_accessor.cells<mesh::y_axis, mesh::range::logical>()), "ghost_cells") {
      val[j][I][1] = 100;
      val[j][I][0] = 100;
    };
  if (gysize != 0)
    forall(i, (mesh_accessor.cells<mesh::x_axis, mesh::range::logical>()), "ghost_cells") {
      val[J][i][1] = 100;
      val[J][i][0] = 100;
    };

  annotation::end<init_task>();
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::init_output_mgr(mesh::accessor<ro> mesh_accessor,
field<std::array<double, 2>>::accessor<ro, ro> field_accessor) {
  output_mgr = std::make_unique<OutputManager>(mesh_accessor, field_accessor);
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
