/**
 * @file print.cpp
 * @author Clément Palézis
 * @brief Print task file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "print.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::io(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  std::stringstream ss;
  ss << filebase;
  if(flecsi::processes() == 1) {
    ss << ".dat";
  }
  else {
    ss << "-" << flecsi::process() << ".dat";
  }

  std::cout << "Removing old file : " << ss.str() << "... ";
  try {
    if(std::filesystem::remove(ss.str())) {
      std::cout << "Deleted !" << std::endl;
    }
    else {
      std::cout << "Not found !" << std::endl;
    }
  }
  catch(const std::filesystem::filesystem_error& err) {
    std::cout << "filesystem error: " << err.what() << std::endl;
  }
  std::cout << "Creating new file : " << ss.str() << std::endl;
  std::ofstream file(ss.str(), std::ofstream::out);

  for(auto j : mesh_accessor.cells<mesh::y_axis>()) {
    for(auto i : mesh_accessor.cells<mesh::x_axis>()) {
      file << val[j][i][0] << "\t";
    }
    file << std::endl;
  }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::global_id(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  std::stringstream ss;
  ss << filebase;
  if(flecsi::processes() == 1) {
    ss << ".dat";
  }
  else {
    ss << "-" << flecsi::process() << ".dat";
  }

  std::cout << "Removing old file : " << ss.str() << "... ";
  try {
    if(std::filesystem::remove(ss.str())) {
      std::cout << "Deleted !" << std::endl;
    }
    else {
      std::cout << "Not found !" << std::endl;
    }
  }
  catch(const std::filesystem::filesystem_error& err) {
    std::cout << "filesystem error: " << err.what() << std::endl;
  }
  std::cout << "Creating new file : " << ss.str() << std::endl;
  std::ofstream file(ss.str(), std::ofstream::out);

  for(auto j : mesh_accessor.cells<mesh::y_axis>()) {
    for(auto i : mesh_accessor.cells<mesh::x_axis>()) {
      // file << val[0][j][i] << "\t";
      file << "(" << mesh_accessor.global_id<mesh::x_axis>(i) << ", " << mesh_accessor.global_id<mesh::y_axis>(j) << ")" << "\t";
    }
    file << std::endl;
  }
}

void
task::id(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase) {
  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  std::stringstream ss;
  ss << filebase;
  if(flecsi::processes() == 1) {
    ss << ".dat";
  }
  else {
    ss << "-" << flecsi::process() << ".dat";
  }

  std::cout << "Removing old file : " << ss.str() << "... ";
  try {
    if(std::filesystem::remove(ss.str())) {
      std::cout << "Deleted !" << std::endl;
    }
    else {
      std::cout << "Not found !" << std::endl;
    }
  }
  catch(const std::filesystem::filesystem_error& err) {
    std::cout << "filesystem error: " << err.what() << std::endl;
  }
  std::cout << "Creating new file : " << ss.str() << std::endl;
  std::ofstream file(ss.str(), std::ofstream::out);

  for(auto j : mesh_accessor.cells<mesh::y_axis>()) {
    for(auto i : mesh_accessor.cells<mesh::x_axis>()) {
      // file << val[0][j][i] << "\t";
      file << "(" << i << ", " << j << ")" << "\t";
    }
    file << std::endl;
  }
}