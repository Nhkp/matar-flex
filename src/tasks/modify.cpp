/**
 * @file modify.cpp
 * @author Clément Palézis
 * @brief Modify task file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "modify.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::set_mesh_and_field(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor)
{
  output_mgr->set_mesh_and_field(mesh_accessor, field_accessor);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void
task::finite_diff(
  mesh::accessor<rw> mesh_accessor,
  field<std::array<double, 2>>::accessor<rw, rw> field_accessor,
  int it) {
  annotation::begin<finite_diff_task>();

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);
  
  forall(j, (mesh_accessor.cells<mesh::y_axis,
    mesh::range::logical>()), "cells") {
    forall(i, (mesh_accessor.cells<
      mesh::x_axis, mesh::range::logical>()), "cells") {
      val[j][i][it % 2] = 0.25 * (val[j][i+1][(it + 1) % 2]
                       + val[j][i-1][(it + 1) % 2]
                       + val[j+1][i][(it + 1) % 2]
                       + val[j-1][i][(it + 1) % 2]);
    };
  };

  int I = mesh_accessor.offset<mesh::x_axis, mesh::range::ghost>();
  int J = mesh_accessor.offset<mesh::y_axis, mesh::range::ghost>();

  int gxsize = mesh_accessor.size<mesh::index_space::cells, mesh::x_axis, mesh::range::ghost>();
  int gysize = mesh_accessor.size<mesh::index_space::cells, mesh::y_axis, mesh::range::ghost>();

  if (gxsize != 0)
    forall(j, (mesh_accessor.cells<mesh::y_axis, mesh::range::interior>()), "ghost_cells") {
      val[j][I][(it + 1) % 2] = val[j][I-gxsize][it % 2];
    };
  if (gysize != 0)
    forall(i, (mesh_accessor.cells<mesh::x_axis, mesh::range::interior>()), "ghost_cells") {
      val[J][i][(it + 1) % 2] = val[J-gysize][i][it % 2];
    };

  annotation::end<finite_diff_task>();
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

double
task::max_diff(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  double precision) {
  annotation::begin<max_diff_task>();

  auto val = mesh_accessor.mdspan<mesh::cells>(field_accessor);

  for(const auto j : mesh_accessor.cells<mesh::y_axis,
    mesh::range::logical>()) {
    for(const auto i : mesh_accessor.cells<mesh::x_axis,
      mesh::range::logical>()) {
      precision =
        fmax(fabs(val[j][i][0] - val[j][i][1]), precision);
    };
  }

  annotation::end<max_diff_task>();
  return precision;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
