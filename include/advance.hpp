/**
 * @file advance.hpp
 * @author Clément Palézis
 * @brief Advance header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "state.hpp"
#include "modify.hpp"
#include "options.hpp"
#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace action {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int action::advance_init()
 * @brief Advance init action
 * 
 * Execute advance_init task.
 * 
 * @return int 
 */
int advance();
inline control::action<advance, cp::advance> advance_action;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int action::advance()
 * @brief Advance action
 * 
 * Executes Modify task on the whole launch domain through MPI.
 * 
 * @return int 
 */
int advance_init();
inline control::action<advance_init, cp::advance> advance_init_action;

inline const auto dep = advance_action.add(advance_init_action);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace action

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
