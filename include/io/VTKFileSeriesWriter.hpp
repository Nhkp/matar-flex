/*---------------------------------------------------------------------------*/
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (C) 2022 Benjamin Fovet                                         */
/*---------------------------------------------------------------------------*/
#pragma once
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "nlohmann/json.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

class VTKFileSeriesWriter
{
public:
  struct VTKFileInfo
  {
    std::string name;
    double time;
    VTKFileInfo() {}
    VTKFileInfo(const std::string& name, double time)
      : name(name), time(time) {}
  };

public:
  /**
   * @brief
   */
  VTKFileSeriesWriter();

  /**
   * @brief
   */
  void append_file(const std::string& vtk_file_path, double time);

  /**
   * @brief
   */
  void write(const std::string& file_series_path);

private:
  nlohmann::json m_root;
};

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void to_json(nlohmann::json& j, const VTKFileSeriesWriter::VTKFileInfo& file_info);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
