/*---------------------------------------------------------------------------*/
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (C) 2022 Clément Palézis                                        */
/*---------------------------------------------------------------------------*/
#pragma once
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <map>
#include <string>
#include <vector>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <fmt/os.h>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "VTKFileSeriesWriter.hpp"
#include "mesh.hpp"
#include "FileUtils.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Writes a mesh to a VTK file using the legacy ASCII format.
 * @note This method is primarily intended for debugging.
 */
class VTKWriter
{
public:
  /**
   * @brief
   * @param name
   */
  VTKWriter(mesh::accessor<ro> mesh_accessor,
    field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
    const std::string& output_directory,
    const std::string& name,
    int rank = 0, int size = 1);

  /**
   *
   * @param fields
   * @param iteration
   */
  int write(const std::map<std::string, std::vector<double>>& fields,
             int iteration = 0, double time = 0.0) const;

private:
  /**
   * @brief Writes a rectilinear mesh to a VTK file using the legacy ASCII format.
   * @param [in] file the stream to write to.
   */
  void write_rectilinear_mesh(fmt::ostream& file) const;

  /**
   * @brief Writes the dimensions of a structured mesh to a
   *      VTK file using the legacy ASCII format.
   * @param [in] mesh the structured mesh to write out.
   * @param [in] file the stream to write to.
   */
  void write_dimensions(fmt::ostream& file) const;

private:
  /// @brief File series writer for ParaView
  std::unique_ptr<VTKFileSeriesWriter> m_file_series_writer;

  /// @brief Mesh accessor
  mesh::accessor<ro> m_mesh;
  
  /// @brief Field accessor
  field<std::array<double, 2>>::accessor<ro, ro> m_field;
  
  /// @brief Output directory.
  std::string m_output_directory;

  /// @brief File name.
  std::string m_name;

  /// @brief MPI process rank.
  int m_rank;

  /// @brief Number of MPI processes.
  int m_size;
};

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
