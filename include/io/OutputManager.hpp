/*---------------------------------------------------------------------------*/
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (C) 2022 Clément Palézis                                        */
/*---------------------------------------------------------------------------*/
#pragma once
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <string>
#include <variant>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "WriterVariant.hpp"
#include "options.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

class OutputManager
{
public:
  OutputManager(
    mesh::accessor<ro> mesh_accessor,
    field<std::array<double, 2>>::accessor<ro, ro> field_accessor);

  /**
   * @brief Set the mesh and field function
   * 
   * @param mesh_accessor 
   * @param field_accessor 
   */
  void set_mesh_and_field(
    mesh::accessor<ro> mesh_accessor,
    field<std::array<double, 2>>::accessor<ro, ro> field_accessor);

  /**
   * @brief
   * @param output_directory
   */
  void set_output_directory(const std::string& output_directory);

  /**
   * @brief
   */
  const std::string& output_directory() const;

  /**
   *
   */
  void force_output();

  /**
   * @brief
   */
  void write(int iteration = 0, double time = 0.0);

private:
  /**
   * @brief
   */
  void register_fields();

  /**
   * @brief
   */
  void register_writer();

  /**
   * @brief
   */
  void copy_fields(int iteration);

  /**
   *
   * @return
   */
  bool activate_output(int iteration, double time);

private:
  /// @brief Output directory path.
  std::string m_output_directory;
  
  /// @brief Whether to force an output.
  bool m_force_output = false;

  /// @brief Map for m_fields to output.
  std::map<std::string, std::vector<double>> m_fields;

  /// @brief Mesh accessor
  mesh::accessor<ro> m_mesh;
  
  /// @brief Field accessor
  field<std::array<double, 2>>::accessor<ro, ro> m_field;

  /// @brief Output writer variant.
  WriterVariant m_writer_variant;
};

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
