/**
 * @file finalize.hpp
 * @author Clément Palézis
 * @brief Finalize header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "options.hpp"
#include "state.hpp"
#include "print.hpp"
#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace action {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int print()
 * @brief Print action
 * 
 * Executes io task, print task if debug option is true.
 * 
 * @return int 
 */
int print();
inline control::action<print, cp::finalize> print_action;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int finalize()
 * @brief Finalize action
 * 
 * Prompts "App Finalization". Execution depends on print action.
 * 
 * @return int 
 */
inline int
finalize() {
  flog(info) << "Application finalization" << std::endl;

  return 0;
}
inline control::action<finalize, cp::finalize> finalize_action;

inline const auto finalize_dep = finalize_action.add(print_action);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace action

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
