/**
 * @file options.hpp
 * @author Clément Palézis
 * @brief User options file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/run/control.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Iterations user option
 * 
 * Defines iterations number user options. User can pass number of iterations
 * [1-10] he wants through command line interface via --nb-iter <nb> options.
 */
inline flecsi::program_option<int> nb_iter("Control Model Options",
  "nb_iter,n",
  "Specify the number of iterations [1-10].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value > 0 && value < 11
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Fault tolerance user option
 * 
 * Defines the delta of temp tolerated
 */
inline flecsi::program_option<double> precision("Control Model Options",
  "precision,p",
  "Specify fault tolerance [0.0001-0.1].",
  {{flecsi::option_default, 0.01}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const double value = flecsi::option_value<double>(v);
    return value <= 0.1 && value >= 0.0001
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Ghost cells user option
 * 
 * Defines the number of ghost layers
 */
inline flecsi::program_option<int> nb_ghosts("Mesh Options",
  "nb-ghosts,g",
  "Specify the number of ghost layers [0-10].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value <= 10 && value >= 0
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Boundary cells user option
 * 
 * Defines the number of boundary layers
 */
inline flecsi::program_option<int> nb_boundaries("Mesh Options",
  "nb-boundaries,b",
  "Specify the number of boundary layers [0-10].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value <= 10 && value >= 0
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Dump frequency user option
 * 
 * Defines interval between two iterations for VTK dumping.
 */
inline flecsi::program_option<int> dump_freq("VTK Options",
  "dump-frequency,f",
  "Specify dump interval [1-100].",
  {{flecsi::option_default, 10}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<int>(v);
    return value > 0 && value < 101
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Enable VTK writer
 * 
 * Enables use of VTK to dump cells' value.
 */
inline flecsi::program_option<std::string> dump_vtk("VTK Options",
  "vtk,v",
  "Enable Debug VTK writer.",
  {{flecsi::option_default, ""}, {flecsi::option_implicit, "debug"}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const std::string value = flecsi::option_value<std::string>(v);
    return value == "" || value == "debug" || value == "normal"
             ? true
             : (ss << "VTK dumping mode not recognized") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Enable VTK writer
 * 
 * Enables use of VTK to dump last iteration cells' value.
 */
inline flecsi::program_option<bool> dump_last("VTK Options",
  "dump-last-iter,d",
  "Enable Debug VTK writer during last iteration.",
  {{flecsi::option_default, false}, {flecsi::option_implicit, true}}); 

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Initialization shape user option
 * 
 * Defines shape which initialize the problem.
 */
inline flecsi::program_option<std::string> init_shape("Control Model Options",
  "shape,s",
  "Specify the initial shape.",
  {{flecsi::option_default, "full"}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const std::string value = flecsi::option_value<std::string>(v);
    return value == "center" || value == "corner" || value == "outline" ||
           value == "full"
             ? true
             : (ss << "shape not recognized") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Output directory name user option
 * 
 * Defines output directory name for VTK data dump.
 */
inline flecsi::program_option<std::string> od_name("Control Model Options",
  "output-dir,o",
  "Specify output directory name.",
  {{flecsi::option_default, ""}}
  );

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief X size user option
 * 
 * Defines x size user options.
 */
inline flecsi::program_option<std::size_t> x_extents("x-extents",
  "The x extents of the mesh [1-32].",
  1);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Y size user option
 * 
 * Defines y size user options.
 */
inline flecsi::program_option<std::size_t> y_extents("y-extents",
  "The x extents of the mesh [1-32].",
  1);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @brief Z size user option
 * 
 * Defines z size user options.
 */
inline flecsi::program_option<std::size_t> z_extents("Mesh Options",
  "z-extents,z",
  "The x extents of the mesh [1-32].",
  {{flecsi::option_default, 1}},
  [](flecsi::any const & v, std::stringstream & ss) {
    const int value = flecsi::option_value<std::size_t>(v);
    return value > 0 && value < 33
             ? true
             : (ss << "value(" << value << ") out-of-range") && false;
  });

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
