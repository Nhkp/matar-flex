/**
 * @file modify.hpp
 * @author Clément Palézis
 * @brief Modify task header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "types.hpp"
#include "mesh.hpp"
#include "state.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline flecsi::log::tag tag_advance("advance");

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void set_mesh_and_field(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void finite_diff(mesh::accessor<wo> mesh_accessor,
 *  mesh::accessor<ro> previous_mesh_accessor,
 *  field<std::array<double, 2>>::accessor<wo, wo> field_accessor,
 *  field<std::array<double, 2>>::accessor<ro, ro> previous_field_accessor)
 * @brief Finite difference task
 * 
 * Compute the finite difference between previous and actual values.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<rw, rw>)
 * @param it (int)
 */
void finite_diff(
  mesh::accessor<rw> mesh_accessor,
  field<std::array<double, 2>>::accessor<rw, rw> field_accessor,
  int it);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

double max_diff(
  mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  double precision);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn inline const char * init()
 * @brief Advance init task dep
 * 
 * Returns "Advance initialization".
 * 
 * @return const char * 
 */
inline const char *
advance_init() {
  return "Advance...";
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
