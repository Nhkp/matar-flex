/**
 * @file print.hpp
 * @author Clément Palézis
 * @brief Print task header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>
#include <sstream>
#include <filesystem>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "types.hpp"
#include "mesh.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline flecsi::log::tag tag_finalize("finalize");

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void io(mesh::accessor<ro> mesh_accessor,
 *   field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
 *   std::string filebase)
 * @brief IO function
 * 
 * Print cells value in a file. Produce one file per process.
 * 
 * @param mesh_accessor (mesh::accessor<ro>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<ro, ro>)
 * @param filebase (std::string)
 */
void io(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void global_id(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase);

void id(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor,
  std::string filebase);

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
