/**
 * @file init.hpp
 * @author Clément Palézis
 * @brief Init task header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "state.hpp"
#include "options.hpp"
#include "types.hpp"
#include "mesh.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace task {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void apply_corner_ic(mesh::accessor<wo> mesh_accessor,
 *  field<std::array<double, 2>>::accessor<wo, wo> field_accessor)
 * @brief Apply initial conditions task
 * 
 * Apply corner shape initial conditions' values.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<wo, wo>)
 */
void apply_corner_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void apply_outline_ic(mesh::accessor<wo> mesh_accessor,
 *  field<std::array<double, 2>>::accessor<wo, wo> field_accessor)
 * @brief Apply initial conditions task
 * 
 * Apply outline shape initial conditions' values.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<wo, wo>)
 */
void apply_outline_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void apply_center_ic(mesh::accessor<wo> mesh_accessor,
 *  field<std::array<double, 2>>::accessor<wo, wo> field_accessor)
 * @brief Apply initial conditions task
 * 
 * Apply center point shape initial conditions' values.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<wo, wo>)
 */
void apply_center_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn void apply_full_ic(mesh::accessor<wo> mesh_accessor,
 *  field<std::array<double, 2>>::accessor<wo, wo> field_accessor)
 * @brief Apply initial conditions task
 * 
 * Apply full filled shape initial conditions' values.
 * 
 * @param mesh_accessor (mesh::accessor<wo>)
 * @param field_accessor (field<std::array<double, 2>>::accessor<wo, wo>)
 */
void apply_full_ic(mesh::accessor<wo> mesh_accessor,
  field<std::array<double, 2>>::accessor<wo, wo> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void init_output_mgr(mesh::accessor<ro> mesh_accessor,
  field<std::array<double, 2>>::accessor<ro, ro> field_accessor);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace task

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
