/**
 * @file mesh.hpp
 * @author Clément Palézis
 * @brief Mesh specification file
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/data.hh>
#include <flecsi/topo/narray/coloring_utils.hh>
#include <flecsi/topo/narray/interface.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "types.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @struct mesh
 * @brief Mesh
 * 
 * Structured mesh
 */
struct mesh : flecsi::topo::specialization<flecsi::topo::narray, mesh> {

  /*-----------------------------------------------------------------------*
    Policy Information.
   *-----------------------------------------------------------------------*/

  enum index_space { cells , nodes };
  using index_spaces = mesh::has<cells, nodes>;

  enum axis { x_axis, y_axis };
  using axes = mesh::has<x_axis, y_axis>;

  enum range { all, interior, global, logical, ghost };
  enum boundary { low, high };

  struct meta_data {
    double xdelta;
    double ydelta;
  };

  static constexpr std::size_t dimension = 2;

  template<auto>
  static constexpr std::size_t privilege_count = 2;

  /*-----------------------------------------------------------------------*
    Interface.
   *-----------------------------------------------------------------------*/

  /**
   * @struct interface
   * @brief Interface struct
   * 
   * A templated interface to manipulate the mesh.
   * 
   * @tparam B (class)
   */
  template<class B>
  struct interface : B {

    /**
     * @fn std::size_t size()
     * @brief Size function
     * 
     * A templated function returning the size of a given index space in a given
     * range along a given axis.
     * 
     * @tparam IS (index_space, default = cells)
     * @tparam A (axis)
     * @tparam SE (range, default = all)
     * @return std::size_t 
     */
    template<index_space IS = cells, axis A, range SE = all>
    std::size_t size() const {
      if(SE == all) {
        return B::template size<IS, A, B::range::all>();
      }
      else if(SE == global) {
        return B::template size<IS, A, B::range::global>();
      }
      else if(SE == logical) {
        return B::template size<IS, A, B::range::logical>();
      }
      else {
        const bool low = B::template is_low<IS, A>();
        const bool high = B::template is_high<IS, A>();

        if(high && low) {
          return 0;
        }
        else if(low) {
          return 0 + B::template 
            size<IS, A, B::range::ghost_high>();
        }
        else {
          return 0 - B::template 
            size<IS, A, B::range::ghost_low>();
        }
      }
    }

    /**
     * @fn std::size_t offset()
     * @brief Offset function
     * 
     * A templated function returning the offset of a given range along a
     * given axis.
     * 
     * @tparam A (axis)
     * @tparam SE (range, default = all)
     * @return std::size_t 
     */
    template<axis A, range SE = all>
    std::size_t offset() const {
      if(SE == all) {
        return B::template offset<index_space::cells, A, B::range::all>();
      }
      else if(SE == global) {
        return B::template offset<index_space::cells, A, B::range::global>();
      }
      else if(SE == logical) {
        return B::template offset<index_space::cells, A, B::range::logical>();
      }
      else if(SE == ghost) {
        const bool low = B::template is_low<index_space::cells, A>();
        const bool high = B::template is_high<index_space::cells, A>();

        if(high && low) {
          return 0;
        }
        else if(low) {
          return B::template offset<index_space::cells, A, B::range::ghost_high>();
        }
        else {
          return B::template offset<index_space::cells, A, B::range::ghost_low>();
        }
      }
    }

    /**
     * @fn auto cells()
     * @brief Range functions
     * 
     * Returns cells' range index space.
     * 
     * @tparam A (axis)
     * @return auto 
     */
    template<axis A, range SE = all>
    auto cells() const {
      if(SE == all) {
        return B::template extents<index_space::cells, A, B::range::all>();
      }
      else if(SE == interior) {
        auto const & md = *(this->meta_);
        const bool low = B::template is_low<index_space::cells, A>();
        const bool high = B::template is_high<index_space::cells, A>();

        const std::size_t start =
          md.logical[index_space::cells][0][A];
        const std::size_t end =
          md.logical[index_space::cells][1][A];

        return flecsi::topo::make_ids<index_space::cells>(
          flecsi::util::iota_view<flecsi::util::id>(start - high, end + low));
      }
      else {
        return B::
          template extents<index_space::cells, A, B::range::logical>();
      }
    }

     /**
     * @fn auto ghosts()
     * @brief Range functions
     * 
     * Returns ghost cells' range index space.
     * 
     * @tparam A (axis)
     * @return auto 
     */
    template<axis A>
    auto ghosts() const {
      auto const & md = *(this->meta_);
      const bool low = B::template is_low<index_space::cells, A>();
      const bool high = B::template is_high<index_space::cells, A>();

        const std::size_t start =
          md.logical[index_space::cells][0][A];
        const std::size_t end =
          md.logical[index_space::cells][1][A];

      if(high && low) {
        return flecsi::topo::make_ids<index_space::cells>(
          flecsi::util::iota_view<flecsi::util::id>(0, 0));
      }
      else if(low) {
        return B::template
          extents<index_space::cells, A, B::range::ghost_high>();
      }
      else {
        return B::template
          extents<index_space::cells, A, B::range::ghost_low>();
      }
    }

    /**
     * @fn auto nodes()
     * @brief Range functions
     * 
     * Returns nodes' range index space.
     * 
     * @tparam A (axis)
     * @return auto 
     */
    template<axis A, range SE = all>
    auto nodes() const {
      if(SE == all) {
        return B::template extents<index_space::nodes, A, B::range::all>();
      }
      else if(SE == interior) {
        auto const & md = *(this->meta_);
        const bool low = B::template is_low<index_space::nodes, A>();
        const bool high = B::template is_high<index_space::nodes, A>();

        const std::size_t start =
          md.logical[index_space::nodes][0][A];
        const std::size_t end =
          md.logical[index_space::nodes][1][A];

        return flecsi::topo::make_ids<index_space::nodes>(
          flecsi::util::iota_view<flecsi::util::id>(start - high, end + low));
      }
      else {
        return B::
          template extents<index_space::nodes, A, B::range::logical>();
      }
    }

     /**
     * @fn std::size_t global_id(std::size_t i) const
     * @brief Global ID function
     * 
     * A templated function returning the ID of a cell along a given axis.
     * 
     * @tparam A (axis)
     * @param i (std::size_t)
     * @return std::size_t 
     */
    template<axis A>
    std::size_t global_id(std::size_t i) const {
      return B::template offset<index_space::cells, A, B::range::global>() + i;
    }

    /**
     * @fn double xdelta()
     * @brief Xdelta function
     * 
     * A function returning the xdelta value.
     * 
     * @return double 
     */
    double xdelta() {
      return (*(this->policy_meta_)).xdelta;
    }

    /**
     * @fn double ydelta()
     * @brief Ydelta function
     * 
     * A function returning the ydelta value.
     * 
     * @return double 
     */
    double ydelta() {
      return (*(this->policy_meta_)).ydelta;
    }

  }; // struct interface

  /**
   * @fn static auto distribute(std::size_t np,
   *       std::vector<std::size_t> indices)
   * @brief Distribute function
   * 
   * A function distributing the number of processes along given indices.
   * 
   * @param np (std::size_t)
   * @param indices (std::vector<std::size_t>)
   * @return auto 
   */
  static auto distribute(std::size_t np, std::vector<std::size_t> indices) {
    return flecsi::topo::narray_utils::distribute(np, indices);
  }

  /*-----------------------------------------------------------------------*
    Color Method.
   *-----------------------------------------------------------------------*/
  
  /**
   * @fn static coloring color(colors axis_colors, coord, axis_extents)
   *  @brief Color function
   * 
   * A function coloring the mesh with a given color along a given axis.
   * 
   * @param axis_colors (colors)
   * @param axis_extents (colors)
   * @return coloring 
   */
  static coloring
  color(base::colors axis_colors,
    base::coord cells_extents,
    base::coord nodes_extents,
    const size_t nb_ghost_layers,
    const size_t nb_boundary_layers) {
    
    base::coord hdepths{nb_ghost_layers, nb_ghost_layers};
    base::coord bdepths{nb_boundary_layers, nb_boundary_layers};
    std::vector<bool> periodic{false, false};

    std::vector<base::coloring_definition> cd{
      {axis_colors, cells_extents, hdepths, bdepths, periodic}};
    auto [ncolors, index_colorings] =
      flecsi::topo::narray_utils::color(cd, MPI_COMM_WORLD);

    std::vector<base::coloring_definition> nodes_cd{
      {axis_colors, nodes_extents , hdepths, bdepths, periodic}};
    auto [nodes_ncolors, nodes_index_colorings] =
      flecsi::topo::narray_utils::color(nodes_cd, MPI_COMM_WORLD);

    flog_assert(ncolors == flecsi::processes(),
      "current implementation is restricted to 1-to-1 mapping");

    coloring c;
    c.comm = MPI_COMM_WORLD;
    c.colors = ncolors;
    for(auto idx : index_colorings) {
      for(auto ic : idx) {
        c.idx_colorings.emplace_back(ic.second);
      }
    }

    for(auto idx : nodes_index_colorings) {
      for(auto ic : idx) {
        c.idx_colorings.emplace_back(ic.second);
      }
    }

    return c;
  }

  /*-----------------------------------------------------------------------*
    Initialization.
   *-----------------------------------------------------------------------*/

  using grect = std::array<std::array<double, 2>, 2>;

  /**
   * @fn static void set_geometry(mesh::accessor<flecsi::rw> sm,
   *       grect const & g)
   * @brief Set Geometry function
   * 
   * A task function initializing the mesh geometry.
   * 
   * @param sm (mesh::accessor<rw>)
   * @param g (std::array<std::array<double, 2>, 2> const &)
   */
  static void set_geometry(mesh::accessor<flecsi::rw> sm, grect const & g) {
    meta_data & md = sm.policy_meta_;
    double xdelta =
      std::abs(g[0][1] - g[0][0]) / (sm.size<cells, x_axis, global>() - 1);
    double ydelta =
      std::abs(g[1][1] - g[1][0]) / (sm.size<cells, y_axis, global>() - 1);

    md.xdelta = xdelta;
    md.ydelta = ydelta;
  }

  /**
   * @fn static void initialize(flecsi::data::topology_slot<mesh> & s,
           coloring const &,
           grect const & geometry)
   * @brief Initialize function
   * 
   * A wrapping function launching set_geometry through mpi processes.
   * 
   * @param s (data::topology_slot<mesh> &)
   * @param geometry (grect const &)
   */
  static void initialize(flecsi::data::topology_slot<mesh> & s,
    coloring const &,
    grect const & geometry) {
    flecsi::execute<set_geometry, flecsi::mpi>(s, geometry);
  }

}; // struct mesh

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
