/**
 * @file state.hpp
 * @author Clément Palézis
 * @brief State header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/util/annotation.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "mesh.hpp"
#include "OutputManager.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline const field<std::array<double, 2>>
  ::definition<mesh, mesh::cells> cell_field;

inline mesh::slot mesh_instance;

inline mesh::cslot coloring;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

inline std::unique_ptr<OutputManager> output_mgr;

inline int iteration = 0;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

using namespace flecsi::util;

struct user_execution : annotation::context<user_execution> {
  static constexpr char name[] = "User-Execution";
};


struct init_region : annotation::region<user_execution> {
  inline static const std::string name{"init_region"};
};

struct init_task : annotation::region<user_execution> {
  inline static const std::string name{"init_task"};
  static constexpr annotation::detail detail_level = annotation::detail::high;
};


struct advance_region : annotation::region<user_execution> {
  inline static const std::string name{"advance_region"};
};

struct finite_diff_task : annotation::region<user_execution> {
  inline static const std::string name{"finite_diff_task"};
  static constexpr annotation::detail detail_level = annotation::detail::high;
};

struct max_diff_task : annotation::region<user_execution> {
  inline static const std::string name{"max_diff_task"};
  static constexpr annotation::detail detail_level = annotation::detail::high;
};

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
