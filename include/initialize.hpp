/**
 * @file initialize.hpp
 * @author Clément Palézis
 * @brief Initialize header
 * @version 0.1
 * @date 27-06-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include <flecsi/flog.hh>

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "options.hpp"
#include "state.hpp"
#include "init.hpp"
#include "control.hpp"

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

namespace action {

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int initialize()
 * @brief Initialize action
 * 
 * Prompts "Application initialization".
 * 
 * @return int 
 */
inline int
initialize() {
  flog(info) << "Application initialization" << std::endl;

  return 0;
}
inline control::action<initialize, cp::initialize> initialize_action;

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/**
 * @fn int init_mesh()
 * @brief Mesh Initialization action
 * 
 * Initialize mesh and execute init_one task. This action depends on
 * initialize action.
 * 
 * @return int 
 */
int init_mesh();
inline control::action<init_mesh, cp::initialize> init_mesh_action;

inline const auto init_dep = init_mesh_action.add(initialize_action);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

} // namespace action

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
