# Matar FleX

**Matar FleX** is a **MATAR** reimplementation through [**FleCSI**](https://github.com/flecsi/flecsi) paradigm.

[**MATAR**](https://github.com/lanl/MATAR) is a C++ library that addresses the need for simple, fast, and memory-efficient multi-dimensional data representations for dense and sparse storage that arise with numerical methods and in software applications.

## Getting started

### Getting the code

Clone the repository

    git clone https://gitlab.com/Nhkp/matar-flex.git

### Dependencies

- GCC 9.4.0/Clang 8.0.1+
- CMake 3.20+
- FleCSI 2.1.0
- Legion 21.03.0+
- Kokkos 3.5.00
- Doxygen
- parMETIS 4.0.3/METIS 5.1.0
- Boost 1.70.0
- OpenMPI 3.1.6+

### Contents

- `src`: source files including `io` and `tasks`.
- `include`: source headers including `io`, `specialization` and `tasks`.

## Usage

### Documentation

    cd doc
    doxygen Doxyfile
    firefox html/index.html

### Configuration

    cmake -S . -B build

### Compilation

    cd build
    make [-j 16]

### Execution

    [mpiexec -n <nb>] ./matar <x> <y> [--debug] [--shape <"s">] [--precision <p>] [--dump-frequency <f>] [--output-dir <"o">]

### Visualization
Only work for non-distributed runs

    ../paraview.sh
