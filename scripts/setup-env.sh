#!/bin/bash

set -o errexit -o pipefail -o noclobber -o nounset

PARTITION="default"
SHOW_HELP=OFF
POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--partition)
            PARTITION="${2}"
            shift # past argument
            shift # past value
            ;;
        -h|--help)
            SHOW_HELP=ON
            shift
            ;;
        -*|--*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS+=("${1}") # save positional arg
            shift # past argument
            ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [ $# -eq 0 ] || [ "${SHOW_HELP}" = "ON" ]; then
    echo "Usage: setup.sh <host> -p/--partition <partition>"
    exit 0
fi

HOST="${POSITIONAL_ARGS[0]}"

echo "HOST      = ${HOST}"
echo "PARTITION = ${PARTITION}"

#=============================================================================
# Utility functions
#=============================================================================

symlink_spack_file () {
    _host=${1}
    _arch=${2}
    _partition=${3}
    ln -sf spack/envs/sites/${_host}/${_arch}/${_partition}/spack.yaml .
}

preload_env () {
    _host=${1}
    _partition=${2}
    if [ "${_host}" = "inti" ]; then
        if [ "${_partition}" = "a100" ] || [ "${_partition}" = "a100-bxi" ]; then
            module load flavor/buildcompiler/nvidia/21.5
        else
            module load flavor/buildcompiler/gcc/11
        fi
        module load flavor/buildmpi/openmpi/4.0
    fi
}

spack_env_activate () {
    spack env activate .
}

spack_concretize () {
    _cmd="spack concretize --force --reuse"
    if [ "${_host}" = "inti" ]; then
        ccc_mprun -n 1 -c 128 -p ${_partition} ${_cmd}
    else
        ${_cmd}
    fi
}

spack_install () {
    _host=${1}
    _partition=${2}
    _cmd="spack install --fail-fast --yes-to-all --no-checksum"
    if [ "${_host}" = "inti" ]; then
        ccc_mprun -n 1 -c 128 -p ${_partition} ${_cmd} -j 128
    else
        ${_cmd} -j 8
    fi
}

spack_generate_modules () {
    spack module tcl refresh -y
}

query_spack_arch () {
    _ccc_host=${1:-false}
    _partition=${2:-${PARTITION}}
    if [ "${_ccc_host}" = true ]; then
        _spack_arch=$(ccc_mprun -p ${_partition} spack arch)
    else
        _spack_arch=$(spack arch)
    fi
    echo ${_spack_arch}
}

setup_env () {
    _host=${1}
    _arch=${2}
    _partition=${3}
    symlink_spack_file ${_host} ${_arch} ${_partition}

    # Load any flavor needed to get complete paths
    preload_env ${_host} ${_partition}

    # Env install
    spack_env_activate
    spack_concretize
    spack_install ${_host} ${_partition}
    spack_generate_modules

    module use --append modules/${_arch}
}

#=============================================================================
# Select the architecture
#=============================================================================

if [ "${HOST}" = "ubuntu" ]; then
    ARCH=$(query_spack_arch)
elif [ "${HOST}" = "inti" ]; then
    ARCH=$(query_spack_arch true ${PARTITION})
fi

#=============================================================================
# Main entry point
#=============================================================================

setup_env ${HOST} ${ARCH} ${PARTITION}
echo "Done setup for '${HOST}@${PARTITION}'"
