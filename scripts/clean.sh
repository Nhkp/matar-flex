#!/bin/bash

set -o errexit -o pipefail -o noclobber -o nounset

rm -Rf install_tree modules .spack-env spack.lock spack.yaml
