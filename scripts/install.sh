#!/bin/bash

set -o errexit -o pipefail -o noclobber -o nounset

PARTITION="default"
SHOW_HELP=OFF
POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--partition)
            PARTITION="${2}"
            shift # past argument
            shift # past value
            ;;
        -h|--help)
            SHOW_HELP=ON
            shift
            ;;
        -*|--*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS+=("${1}") # save positional arg
            shift # past argument
            ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [ $# -eq 0 ] || [ "${SHOW_HELP}" = "ON" ]; then
    echo "Usage: setup.sh <host> -p/--partition <partition>"
    exit 0
fi

HOST="${POSITIONAL_ARGS[0]}"

echo "HOST      = ${HOST}"
echo "PARTITION = ${PARTITION}"

#=============================================================================
# Utility functions
#=============================================================================

spack_env_activate () {
  spack env activate . -p
}

load_modules () {
  _host=${1}
  _arch=${2}

  module use --append modules/${_arch}
  module load cmake/3.22.2
  spack load adiak@0.2.2

  if [ "${_host}" = "inti" ]; then
    . scripts/load-inti-linux-rhel8-zen2.sh
  else
    #TODO
    echo "no modules loaded for ubuntu"
  fi
}

build () {
  cmake -S . -B build
}

compile () {
  _host=${1}
  _partition=${2}
  _cmd="make -C build"

  if [ "${_host}" = "inti" ]; then
    ccc_mprun -n 1 -c 128 -p ${_partition} ${_cmd} -j 128
  else
    ${_cmd} -j 16
  fi
}

query_spack_arch () {
    _ccc_host=${1:-false}
    _partition=${2:-${PARTITION}}
    if [ "${_ccc_host}" = true ]; then
        _spack_arch=$(ccc_mprun -p ${_partition} spack arch)
    else
        _spack_arch=$(spack arch)
    fi
    echo ${_spack_arch}
}

install () {
  _host=${1}
  _arch=${2}
  _partition=${3}
  spack_env_activate
  load_modules ${_host} ${_arch}
  build
  compile ${_host} ${_partition}
}

#=============================================================================
# Select the architecture
#=============================================================================

if [ "${HOST}" = "ubuntu" ]; then
    ARCH=$(query_spack_arch)
elif [ "${HOST}" = "inti" ]; then
    ARCH=$(query_spack_arch true ${PARTITION})
fi

#=============================================================================
# Main entry point
#=============================================================================

install ${HOST} ${ARCH} ${PARTITION}
echo "Done install for '${HOST}@${PARTITION}'"
