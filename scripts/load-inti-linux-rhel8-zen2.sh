#!/bin/bash

module use --append modules/linux-rhel8-zen3/

# Inti flavor modules
module load flavor/buildcompiler/gcc/11
module load flavor/buildmpi/openmpi/4.0
module load gnu/11.1.0
module load mpi/openmpi/4.0.5

# Spack environment modules
module load flecsi/2.1.0-gcc-11.1.0-mpi
