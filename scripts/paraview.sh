#!/bin/bash

arg=$(echo '--data=')

prefix=$(ls -td -- data/*/ | head -n 1 | sed -e 's/\:/\\\:/g' | sed -e 's/\ /\\\ /g')

if [ -d "$prefix$(echo 'vtk/heat_equation_1_0000')" ]; then
  suffix=$(echo 'vtk/heat_equation_1_0000/heat_equation_1_0000_..vtk')
else
  suffix=$(echo 'vtk/debug_heat_equation_1_0000/debug_heat_equation_1_0000_..vtk')
fi

echo 'Vizualizing '$prefix$suffix

paraview $arg$prefix$suffix